import { Component, OnInit } from '@angular/core';

import { SearchService } from './search.service';
import { RepositoryService } from '../shared/repository.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchQuery: string;
  isLoading = false;
  isDataSent = false;

  constructor(private searchService: SearchService,
              private repoService: RepositoryService) { }

  ngOnInit() {
    if (this.repoService.getRepositories().length) {
      this.isDataSent = true;
    }
  }

  onSearch() {
    this.isLoading = true;
    this.isDataSent = false;
    this.searchService.getRepositories(this.searchQuery)
      .subscribe(data => {
        this.isLoading = false;
        this.isDataSent = true;
        this.repoService.setRepositories(data['items']);
      });
  }

}
