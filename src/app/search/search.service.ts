import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class SearchService {

  private baseSearchUrl = 'https://api.github.com/search/repositories?q=';

  constructor(private http: HttpClient) { }

  public getRepositories(name: string) {
    const requestUrl = this.baseSearchUrl + name + '&per_page=100';
    return this.http.get(requestUrl);
  }
}
