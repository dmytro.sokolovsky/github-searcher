import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { DataScrollerModule, ProgressSpinnerModule } from 'primeng/primeng';

import { SearchComponent } from './search.component';
import { SearchListComponent } from './search-list/search-list.component';

import { SearchService } from './search.service';

@NgModule({
  declarations: [
    SearchComponent,
    SearchListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    DataScrollerModule,
    ProgressSpinnerModule
  ],
  providers: [ SearchService ]
})
export class SearchModule {

}
