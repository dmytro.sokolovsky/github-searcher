import { Component, OnInit } from '@angular/core';
import { RepositoryService } from '../../shared/repository.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {

  repositories: any[];
  languages = [];

  constructor(private repoService: RepositoryService) { }

  ngOnInit() {
    this.repositories = this.repoService.getRepositories();
    if (this.repositories) {
      this.setLanguages(this.repositories);
    }
  }

  langSelected(selectedLang) {
    if (selectedLang === 'all') {
      this.repositories = this.repoService.getRepositories();
      return;
    }
    this.repositories = this.repoService
      .getRepositories()
      .filter(repo => repo['language'] === selectedLang);
  }

  setLanguages(data) {
    const uniqueLangs = {};
    data.map(repo => repo.language)
      .forEach(lang => uniqueLangs[lang] = true);
    this.languages = Object.keys(uniqueLangs);
  }

}
