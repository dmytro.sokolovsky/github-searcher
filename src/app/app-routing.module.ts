import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SearchComponent } from './search/search.component';
import { RepositoryDetailComponent } from './repository-detail/repository-detail.component';

const appRoutes: Routes = [
  { path: '', component: SearchComponent },
  { path: 'repository/:id', component: RepositoryDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
