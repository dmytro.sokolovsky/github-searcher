import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RepositoryService {

  private repositories = [];
  private requestUrl = 'https://api.github.com/repositories/';

  constructor(private http: HttpClient) {
  }

  public setRepositories(repositories: object[]) {
    this.repositories = [...repositories];
  }

  public getRepositories(): object[] {
    return [...this.repositories];
  }

  public getRepositoryById(id) {
    return this.http.get(this.requestUrl + id);
  }
}
