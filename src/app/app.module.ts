import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { ProgressSpinnerModule } from 'primeng/primeng';

import { AppRoutingModule } from './app-routing.module';
import { SearchModule } from './search/search.module';

import { AppComponent } from './app.component';
import { RepositoryDetailComponent } from './repository-detail/repository-detail.component';

import { SearchService } from './search/search.service';
import { RepositoryService } from './shared/repository.service';

@NgModule({
  declarations: [
    AppComponent,
    RepositoryDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ProgressSpinnerModule,
    SearchModule
  ],
  providers: [ RepositoryService ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
