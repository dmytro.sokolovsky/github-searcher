import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RepositoryService } from '../shared/repository.service';

@Component({
  selector: 'app-repository-detail',
  templateUrl: './repository-detail.component.html',
  styleUrls: ['./repository-detail.component.css']
})
export class RepositoryDetailComponent implements OnInit {

  repository: object;


  constructor(private route: ActivatedRoute,
              private repoService: RepositoryService) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.repoService.getRepositoryById(id)
      .subscribe(repo => this.repository = repo);
  }

}
